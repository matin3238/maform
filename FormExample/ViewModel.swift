//
//  ViewModel.swift
//  FormExample
//
//  Created by Matin Abdollahi on 5/12/20.
//  Copyright © 2020 TESTAPP. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ViewModel {
    
    let formGroup: FormGroup
    
    init() {
        self.formGroup = FormGroup(fields: [
            "username" : FormField(initialValue: "", defaultValue: "", validators: [EmailValidator(), RequiredValidator()]),
            "mobile" : FormField(initialValue: "09", defaultValue: "", validators: [MobileNumberValidator(), RequiredValidator()]),
            "password" : FormField(initialValue: "", defaultValue: "", validators: [RequiredValidator(), MinLengthValidator(4), MaxLengthValidator(25)]),
            "rememberMe" : FormField(initialValue: true, defaultValue: false),
            "bank": FormField(initialValue: 0, defaultValue: -1),
        ])
    }
}
