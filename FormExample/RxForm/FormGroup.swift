//
//  FormGroup.swift
//  FormExample
//
//  Created by Matin Abdollahi on 5/12/20.
//  Copyright © 2020 TESTAPP. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa




public class FormGroup {
    
    public lazy var isValid: Observable<Bool> = {
        Observable<Bool>.combineLatest(self._fields.values.map { $0.isValid }) { (elements) -> Bool in
            elements.allSatisfy { $0 }
        }.share()
    }()
    
    private var _fields: [String : FormFieldProtocol]

    
    public init(fields: [String : FormFieldProtocol]) {
        self._fields = fields
    }
    
    public func addField(key: String, field: FormFieldProtocol) {
        self._fields[key] = field
    }
    
    public func reset() {
        for field in (_fields) {
            field.value.reset()
        }
    }
    
    public func getField<T>(_ t: T.Type, key: String) -> FormField<T> {
        return _fields[key]! as! FormField<T>
    }

}
