//
//  ViewController.swift
//  FormExample
//
//  Created by Matin Abdollahi on 5/12/20.
//  Copyright © 2020 TESTAPP. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var errorsLbl: UILabel!
    @IBOutlet weak var usernameTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bankSegment: UISegmentedControl!
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    
    let viewModel = ViewModel()
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let username = viewModel.formGroup.getField(String.self, key: "username")
        let mobile = viewModel.formGroup.getField(String.self, key: "mobile")
        let password = viewModel.formGroup.getField(String.self, key: "password")
        let rememberMe = viewModel.formGroup.getField(Bool.self, key: "rememberMe")
        let bank = viewModel.formGroup.getField(Int.self, key: "bank")
        
        username.drive(usernameTxtField.rx.text).disposed(by: bag)
        self.usernameTxtField.rx.text.orEmpty.bind(to: username).disposed(by: bag)
        
        mobile.drive(mobileTxtField.rx.text).disposed(by: bag)
        self.mobileTxtField.rx.text.orEmpty.bind(to: mobile).disposed(by: bag)
        
        password.drive(passwordTxtField.rx.text).disposed(by: bag)
        self.passwordTxtField.rx.text.orEmpty.bind(to: password).disposed(by: bag)
        
        rememberMe.drive(self.rememberMeSwitch.rx.isOn).disposed(by: bag)
        self.rememberMeSwitch.rx.isOn.bind(to: rememberMe).disposed(by: bag)
        
        bank.drive(bankSegment.rx.selectedSegmentIndex).disposed(by: bag)
        self.bankSegment.rx.selectedSegmentIndex.bind(to: bank).disposed(by: bag)
        

        self.viewModel.formGroup.isValid.asDriver(onErrorJustReturn: false).drive(loginButton.rx.isEnabled).disposed(by: bag)


        Observable<Void>.combineLatest([username.hasError(EmailValidator.name),
                                        username.hasError(RequiredValidator.name),
                                        password.hasError(RequiredValidator.name),
                                        password.hasError(MinLengthValidator.name),
                                        password.hasError(MaxLengthValidator.name),
                                        mobile.hasError(RequiredValidator.name),
                                        mobile.hasError(MobileNumberValidator.name)]) { (result) in
                                        
                                            var str = ""
                                            if (result[0]) {
                                                str += "• email address is wrong\n"
                                            }
                                            if (result[1]) {
                                                str += "• email required\n"
                                            }
                                            if (result[2]) {
                                                str += "• password required\n"
                                            }
                                            if (result[3]) {
                                                str += "• password must contain at least 4 letters\n"
                                            }
                                            if (result[4]) {
                                                str += "• password must contain a maximum of 25 letters\n"
                                            }
                                            if (result[5]) {
                                                str += "• mobile No. required\n"
                                            }
                                            if (result[6]) {
                                                str += "• mobile No. is wrong\n"
                                            }
                                            self.errorsLbl.text = str
                                            
            }.subscribe().disposed(by: bag)
        
    }

    @IBAction func resetFormTapped(_ sender: Any) {
        self.viewModel.formGroup.reset()
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
    }
}

